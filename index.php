<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="theme-color" content="#662383">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ENA vitrine proto</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <header>
        <div class="diapo">
            <img class="photo" src="photos/musee-vue-haut.jpg" alt="diaporama">
            <img class="logo" src="Freycenet la tour_icone app.svg" alt="logo de l'Espace Numérique">
        </div>
        <div class="title">
            <img src="logotext.svg" alt="Espace Numérique et d'Accueil">
        </div>
    </header>
    <div class="catergories-container">
        <div class="category">
            <div class="category-title">
                <h2>Musée Numérique</h2>
                <div class="logo">
                    <img src="museenum.svg" alt="logo musée">
                </div>
            </div>
            <div class="content-container">
                <div class="activity-container">
                    <div class="activity">
                        <h3>Films et diaporamas documentaires</h3>
                        <div class="p-container">
                            <div class="illustration">
                                <img class="video-frame" src="cadre tv.png" alt="frame">
                            </div>
                            <div class="text">
                                <h4>Le village</h4>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut consectetur ligula. Suspendisse vulputate lectus ipsum, et iaculis dui eleifend a. Donec nec volutpat mi. Donec et nibh nec magna euismod sollicitudin. Quisque urna massa, cursus nec fringilla ut, finibus a lectus. Nullam lorem dolor, sagittis sit amet vestibulum vel.
                            </div>
                        </div>
                        <hr>
                        <div class="p-container">
                            <div class="illustration">
                                <img class="video-frame" src="cadre tv.png" alt="frame">
                            </div>
                            <div class="text">
                                <h4>La Nature</h4>
                                <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin congue vitae enim sed convallis. Fusce cursus quis lorem a posuere. Sed sed risus sed erat tincidunt dapibus vel nec metus. Curabitur ut lacinia felis, sed dapibus nisl. Nam eget purus nulla. Nullam in tellus libero. Nunc ultrices magna est, tincidunt.
                            </div>
                        </div>
                        <hr>
                        <div class="p-container">
                            <div class="illustration">
                                <img class="video-frame" src="cadre tv.png" alt="frame">
                            </div>
                            <div class="text">
                                <h4>Les énergies</h4>
                                <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut aliquam eros ut felis ultrices dictum. In hac habitasse platea dictumst. Nunc mauris mauris, finibus semper bibendum ullamcorper, gravida nec dolor. Ut posuere dolor et ex pretium euismod. Nam ut massa leo. Morbi dignissim, eros non mollis venenatis, elit leo consectetur.
                            </div>
                        </div>
                    </div>
                    <div class="photos">
                        <img src="" alt="">
                        <img src="" alt="">
                        <img src="" alt="">
                        <img src="" alt="">
                    </div>
                </div>
                <div class="activity-container">
                    <div class="activity">
                        <h3>Films et diaporamas documentaires</h3>
                        <div class="p-container">
                            <div class="illustration">
                                <img class="video-frame" src="cadre tv.png" alt="frame">
                            </div>
                            <div class="text">
                                <h4>Le village</h4>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut consectetur ligula. Suspendisse vulputate lectus ipsum, et iaculis dui eleifend a. Donec nec volutpat mi. Donec et nibh nec magna euismod sollicitudin. Quisque urna massa, cursus nec fringilla ut, finibus a lectus. Nullam lorem dolor, sagittis sit amet vestibulum vel.
                            </div>
                        </div>
                        <hr>
                        <div class="p-container">
                            <div class="illustration">
                                <img class="video-frame" src="cadre tv.png" alt="frame">
                            </div>
                            <div class="text">
                                <h4>La Nature</h4>
                                <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin congue vitae enim sed convallis. Fusce cursus quis lorem a posuere. Sed sed risus sed erat tincidunt dapibus vel nec metus. Curabitur ut lacinia felis, sed dapibus nisl. Nam eget purus nulla. Nullam in tellus libero. Nunc ultrices magna est, tincidunt.
                            </div>
                        </div>
                        <hr>
                        <div class="p-container">
                            <div class="illustration">
                                <img class="video-frame" src="cadre tv.png" alt="frame">
                            </div>
                            <div class="text">
                                <h4>Les énergies</h4>
                                <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut aliquam eros ut felis ultrices dictum. In hac habitasse platea dictumst. Nunc mauris mauris, finibus semper bibendum ullamcorper, gravida nec dolor. Ut posuere dolor et ex pretium euismod. Nam ut massa leo. Morbi dignissim, eros non mollis venenatis, elit leo consectetur.
                            </div>
                        </div>
                    </div>
                    <div class="photos">
                        <img src="" alt="">
                        <img src="" alt="">
                        <img src="" alt="">
                        <img src="" alt="">
                    </div>
                </div>
                <div class="activity-container">
                    <div class="activity">
                        <h3>Films et diaporamas documentaires</h3>
                        <div class="p-container">
                            <div class="illustration">
                                <img class="video-frame" src="cadre tv.png" alt="frame">
                            </div>
                            <div class="text">
                                <h4>Le village</h4>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut consectetur ligula. Suspendisse vulputate lectus ipsum, et iaculis dui eleifend a. Donec nec volutpat mi. Donec et nibh nec magna euismod sollicitudin. Quisque urna massa, cursus nec fringilla ut, finibus a lectus. Nullam lorem dolor, sagittis sit amet vestibulum vel.
                            </div>
                        </div>
                        <hr>
                        <div class="p-container">
                            <div class="illustration">
                                <img class="video-frame" src="cadre tv.png" alt="frame">
                            </div>
                            <div class="text">
                                <h4>La Nature</h4>
                                <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin congue vitae enim sed convallis. Fusce cursus quis lorem a posuere. Sed sed risus sed erat tincidunt dapibus vel nec metus. Curabitur ut lacinia felis, sed dapibus nisl. Nam eget purus nulla. Nullam in tellus libero. Nunc ultrices magna est, tincidunt.
                            </div>
                        </div>
                        <hr>
                        <div class="p-container">
                            <div class="illustration">
                                <img class="video-frame" src="cadre tv.png" alt="frame">
                            </div>
                            <div class="text">
                                <h4>Les énergies</h4>
                                <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut aliquam eros ut felis ultrices dictum. In hac habitasse platea dictumst. Nunc mauris mauris, finibus semper bibendum ullamcorper, gravida nec dolor. Ut posuere dolor et ex pretium euismod. Nam ut massa leo. Morbi dignissim, eros non mollis venenatis, elit leo consectetur.
                            </div>
                        </div>
                    </div>
                    <div class="photo">
                        <img src="" alt="">
                        <img src="" alt="">
                        <img src="" alt="">
                        <img src="" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="category">
            <div class="category-title">
                <h2>Salle d'Animation</h2>
                <div class="logo"></div>
            </div>
            <div class="content-container">
                <div class="activity-container">
                        <div class="activity">
                            <h3>Films et diaporamas documentaires</h3>
                            <div class="p-container">
                                <div class="illustration">
                                    <img class="video-frame" src="cadre tv.png" alt="frame">
                                </div>
                                <div class="text">
                                    <h4>Le village</h4>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut consectetur ligula. Suspendisse vulputate lectus ipsum, et iaculis dui eleifend a. Donec nec volutpat mi. Donec et nibh nec magna euismod sollicitudin. Quisque urna massa, cursus nec fringilla ut, finibus a lectus. Nullam lorem dolor, sagittis sit amet vestibulum vel.
                                </div>
                            </div>
                            <hr>
                            <div class="p-container">
                                <div class="illustration">
                                    <img class="video-frame" src="cadre tv.png" alt="frame">
                                </div>
                                <div class="text">
                                    <h4>La Nature</h4>
                                    <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin congue vitae enim sed convallis. Fusce cursus quis lorem a posuere. Sed sed risus sed erat tincidunt dapibus vel nec metus. Curabitur ut lacinia felis, sed dapibus nisl. Nam eget purus nulla. Nullam in tellus libero. Nunc ultrices magna est, tincidunt.
                                </div>
                            </div>
                            <hr>
                            <div class="p-container">
                                <div class="illustration">
                                    <img class="video-frame" src="cadre tv.png" alt="frame">
                                </div>
                                <div class="text">
                                    <h4>Les énergies</h4>
                                    <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut aliquam eros ut felis ultrices dictum. In hac habitasse platea dictumst. Nunc mauris mauris, finibus semper bibendum ullamcorper, gravida nec dolor. Ut posuere dolor et ex pretium euismod. Nam ut massa leo. Morbi dignissim, eros non mollis venenatis, elit leo consectetur.
                                </div>
                            </div>
                        </div>
                        <div class="photos">
                            <img src="" alt="">
                            <img src="" alt="">
                            <img src="" alt="">
                            <img src="" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>